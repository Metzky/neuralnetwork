V této složce můžete nalézt vlastní obrázky, které lze použít na vyzkoušení přesnosti Vámi natrénované sítě. Síť je trénovaná na obrázcích o velikosti 28x28 pixelů, proto také doporučuji vytvářet obrázky o této velikosti. Jinak může být neuronová síť daleko nepřesnější. Ale pro zkoušku jsem zde nechal i obrázky o jiných velikostí.

Zde můžete nalézt již předtrénovaná data, která vždy ukládám takto:
parameters1(784, 64, 10;300;relu;Gradient Descent with Momentum).npy
parameters#1(    #2    ; #3; #4;             #5                ).npy

#1 - V případě, že uživatel bude chtít trénovat neuronovou síť o úplně stejných parametrech se toto číslo se zvětší, aby se zabránilo přepsání.
#2 - Definuje velikost sítě. Vás bude především zajímat prostřední číslo, jelikož to jako jediné lze změnit.
#3 - Určuje počet zopakování. Čím vyšší číslo, tím se trénovala delší dobu a tím by teoreticky MĚLA být přesnější (ovšem né nutně).
#4 - Aktivační funkce - Defaultně je možné si vybrat ze 4 možností - sigmoid, relu, tanh, swish. Já mám nejradši relu a tanh. Swish může někdy malinko zlobit.
#5 - Optimalizátor - V základu je možné si vybrat ze 3 možností - Gradient Descent, Gradient Descent with Momentum a Adam. Adam a Gradient Descent with Momentum se mi zdá nejlepší, ale potřebujete na to mít velice malou rychlost učení (zhruba kolem 0.01 - 0.05).
