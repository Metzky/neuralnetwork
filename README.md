# NeuralNetwork

Pro aktivování mého maturitního projektu je zapotřebí nainstalovat program Anaconda.
(https://www.anaconda.com/distribution/#download-section)

Jakmile se tento program nainstaluje, měl byste být schopen otevřít program "jupyter notebook (Anaconda3)". Otevře se nejdříve jednoduchý terminál a po chvilce se otevře prohlížeč složek na localhostu. Zde nalezněte a otevřete můj maturitní projekt (má .ipynb koncovku). 




## ============ Vítejte v mém maturitním projektu ============

Nyní před sebou vidíte řádky kódu v takových buňkách. Abyste kód v jednotlivé buňce aktivovali, musíte do buňky kliknout a dát SHIFT + ENTER.
V první buňce byste měli vidět kód typu: "!pip install tensorflow". Tento kód nainstaluje potřebné knihovny, aby program jako celek mohl správně fungovat. Každé knihovně chvilku trvá, aby se nainstalovala, proto instalace zhruba 7 knihoven bude chvilku trvat. (Některé knihovny mohli být již naintalovány instalací Anacondy, ale raději je tam dávám znova). 

Nyní by měli být všechny knihovny nainstalované a program by měl být schopen fungovat. Teď dávejte SHIFT + ENTER, dokuď nenarazíte na konec programu. V tento moment by se před Vámi mělo objevit jednoduché UI.

Toto UI se skládá ze dvou hlavních "tabulek". Tabulka nahoře slouží k zahájení trénování, zatímco tabulka dole slouží k predikci vlastního obrázku, díky nahrání již predtrénovaných dat.

### Horní UI:
V horním UI lze naleznout 4 složky - "Základ", "Optimalizační funkce", "Pokročilé" a "Dodatečné". Kde v každé složce je vždy nastavení jak samotné sítě, nebo toho, co se bude při trénování ukazovat.

#### Základ:
##### Velikost sítě:
Zde je pouze možné změnit prostřední číslo. V základu je číslo 64. Obecně se doporučuje používat čísla mocniny dvou (2, 4, 8, 16, 32, 64, 128, …).

##### Počet zopakování:
Definuje kolikrát síť "projede" všechna vložená data.

#### Optimalizační funkce:
##### Optimalizace:
Zde je možné si vybrat ze 3 možností: Gradient Descent, Gradient Descent with Momentum a Adam.
Ze zkušeností je lepší Adam nebo Gradient Descent with Momentum. U těchto optimalizačních funkcí se ale doporučuje dávat nižší rychlost učení (o které budu mluvit později).

#### Pokročilé:
##### Aktivační funkce:
Na výběr je z sigmoid / relu / tanh / swish. Nejraději používám relu.

##### Rychlost učení:
Definuje jak rychle se bude neuronová síť učit. Moc velké číslo způsobí, že neuronová síť nebude schopna naleznou optimální minimum, zatímco moc malé číslo způsobí velkou pomalost trénování.
* Gradient Descent:
	* Rychlost učení zhruba 0.5 - 1.5
* Gradient Descent with Momentum / Adam:
	* Rychlost učení nižší než u Gradient Descent - 0.01 - 0.09

#### Dodatečné:
##### Chcete cost?:
Tato možnost určuje, jak často se mají ukazovat výsledky z trénování. Když tedy uživatel zaškrtne možnost: "Každé 100 zopakování", tak se informace o průběhu trénovaní vypíšou každé 100 zopakování.
Rozhodl jsem se ukazovat tři informace – cost, čas za jedno "projetí" všech čísel a přesnost v procentech.

##### Která data?:
V této části jsou dvě možnosti, trénovací a testovací. Na otestování funkčnosti sítě se používají data testovací, jelikož v této sadě je pouze 10 000 obrázků. Je tedy vhodný pro jednoduchou demonstraci neuronové sítě (jelikož se na nich neuronová síť trénuje relativně rychle).
Zatímco data trénovací slouží k trénování sítě. Místo 10 000 obrázku je zde teď 60 000 a každá iterace je o to pomalejší.

##### Ukázat několik příkladů?:
Rozhodl jsem se ještě vytvořit tuto možnost, aby uživatel mohl přesně vidět, co si právě neuronová sít myslí o aktuálních obrázcích. Na začátku se vybere náhodně 5 obrázků, které se každou iteraci kontrolují. V PŘÍPADĚ, že všech 5 obrázků je uhodnuto, vyberou se náhodně nové. 


#### Dolní UI:
Jak jsem již výše říkal, dolní UI slouží k otestování vlastního obrázku.

##### Vlastní obrázek:
* Tlačítko Parametry:
	* Do tohoto tlačítka nahrávejte již předtrénovaná nebo Vámi natrénované parametry. Pozor, soubor musí mít koncovku .npy

* Textbox images/:
	* V tomto textovém poli si volíte obrázek, který se má zpracovat. Obrázky se vybírají ze složky images. Všechny mnou vytvořené obrázky mají jméno myImage#.png => kde # znamená číslo 1-18. Obrázky 1 a 11-18 jsou POUZE velikosti 28x28, zbytek je dělaný na jiných velikostech.


